ARG PRIVATE_REG=""
ARG TZ=UTC

FROM $PRIVATE_REG"debian:11.5-slim"

# Metadata
LABEL \
    version="11.5-stable" \
    maintainer="SmartGeoSystems" \
    description="QGIS Server Stable"

ARG TZ=UTC

ENV \
    # Common environment specific
    DEBIAN_FRONTEND=noninteractive \
    DEBCONF_NONINTERACTIVE_SEEN=true

RUN set -ex \
    && ln -snf /usr/share/zoneinfo/$TZ /etc/localtime \
    #&& dpkg-reconfigure -f noninteractive tzdata \
    && apt-get update -yqq \
    # Install common utils
    && apt-get install --no-install-recommends --no-install-suggests -yqq \
      wget  \
      gnupg  \
      ca-certificates  \
      lsb-release

COPY qgis.sources /etc/apt/sources.list.d/qgis.sources
COPY sources.list /etc/apt/sources.list

RUN set -ex \
    # Set official qgis repo \
    && mkdir -m755 -p /etc/apt/keyrings \
    && wget -q -O /etc/apt/keyrings/qgis-archive-keyring.gpg https://download.qgis.org/downloads/qgis-archive-keyring.gpg \
    && apt-get update -yqq \
    # Upgrade all packages
    && apt-get upgrade -yqq \
    # Install deps
    && apt-get install --no-install-recommends --no-install-suggests -yqq \
      qgis-server \
      spawn-fcgi \
      xauth \
      xvfb \
    # Install microsoft fonts \
    && echo ttf-mscorefonts-installer msttcorefonts/accepted-mscorefonts-eula select true | debconf-set-selections \
    && apt-get install --no-install-recommends -yqq ttf-mscorefonts-installer \
    # Clean \
    && apt-get remove --purge -yqq \
        gnupg \
        wget \
    && apt-get autoremove -yqq \
    && rm -rf \
      /var/lib/apt/lists/* \
      /tmp/* \
      /var/tmp/* \
      /usr/share/man \
      /usr/share/doc \
      /usr/share/doc-base

# Service volumes
VOLUME /tmp
VOLUME /root

ENV \
    QGIS_PREFIX_PATH=/usr \
    QGIS_SERVER_LOG_STDERR=1 \
    QGIS_SERVER_LOG_LEVEL=2 \
    QGIS_SERVER_PARALLEL_RENDERING=1 \
    QGIS_SERVER_MAX_THREADS=4 \
    QGIS_SERVER_IGNORE_BAD_LAYERS=true \
    QGIS_SERVER_LOG_FILE=/var/log/qgis-server.log


#CMD "/usr/bin/xvfb-run -a -n=1 /usr/bin/spawn-fcgi -p 8000 -n /usr/lib/cgi-bin/qgis_mapserv.fcgi"
CMD "qgis_mapserver"

EXPOSE 8000

