# QGIS Server

QGIS Server container.


Ubuntu 20.04 based:
- QGIS Server LTR
- QGIS Server Stable

Ubuntu 22.04 based:
- QGIS Server LTR
- QGIS Server Stable

Debian 11.5 (bullseye) based:
- QGIS Server LTR
- QGIS Server Stable

Packages:

- qgis-server
- spawn-fcgi
- xauth
- xvfb
- ca-certificates
- ttf-mscorefonts